public class HelloWorld 
{
    public String description;

    public HelloWorld() {
        this.description = "Hello World class";
    }

    public void print() {
        System.out.println(message());
    }

    public String message() {
        return "Hello World!";
    }

    public static void main(String[] args) {
        new HelloWorld().print();
    }
}
